import React, { Component } from 'react';
import './Counter.css'
class Counter extends Component {

    constructor() {
        super();
        //That's how we define the state.
        this.state = {
            counter: 0
        }
        //If you are using arrow functions, you don't need to do binding.
        this.increment = this.increment.bind(this);
    }

   //render() method, using arrow function. 
    //render =() =>{
        render () {
            //const style = {fontSize : "50px", padding : "15px 30px"};
        return (
            <div className="counter">
                <button onClick={this.increment}> +{this.props.by}</button>
                {/* Jsx in line styling
                <span className="count" style = {style}>{this.state.counter}</span>*/}            
                <span className="count">{this.state.counter}</span>
            </div>
        );
    }

    increment () {
    //increment =() =>{
        this.setState({
            counter: this.state.counter + this.props.by
        })
    }

}

export default Counter;